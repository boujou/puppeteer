const puppeteer = require("puppeteer");
const nodemailer = require("nodemailer");

require("dotenv").config();

const url =
  "https://www.cdiscount.com/informatique/processeurs/processeur-intel-core-i9-9900k/f-1076443-bx80684i99900k.html";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  await page.setViewport({
    width: 1200,
    height: 10000,
  });

  /*     // pdf
  await page.pdf({
    path: "page.pdf",
    format: "A4",
  });
 */

  /*   // image
  await page.screenshot({
      path: "image.png",
  })
 */

  /*   // get body
  let bodyHTML = await page.evaluate(() => document.body.innerHTML);
  console.log(bodyHTML);
 */

  // get data
  let data = await page.evaluate(() => {
    return document.querySelector("span[itemprop=price]").innerText;
  });
  console.log("le prix est de : " + data);

  let newData = data.substring(0, 3);

  if (parseInt(newData) < 380) {
    console.log("Yes !!!le prix est de : " + newData + " euros");
    sendNotification(newData);
  }

  async function sendNotification(price) {
    let transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS,
        clientId: process.env.OAUTH_CLIENTID,
        clientSecret: process.env.OAUTH_CLIENT_SECRET,
        refreshToken: process.env.OAUTH_REFRESH_TOKEN,
      },
    });

    let info = await transporter
      .sendMail({
        from: '"Processeur CDiscount" <jeanbaptistejosselin@gmail.com>',
        to: "jeanmichel.josselin@free.fr",
        subject: "Prix sous les " + price + " Euros",
        html:
          "Le prix du processeur est en dessous de " +
          price +
          " Euros. " +
          <br /> +
          "Dis moi si ça marche ;)",
      })
      .then(() => console.log("Message envoyé !"));
  }

  await browser.close();
})();
