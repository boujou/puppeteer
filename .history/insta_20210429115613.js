const puppeteer = require("puppeteer");
const nodemailer = require("nodemailer");

require("dotenv").config();

const url =
  "https://www.om/informatique/processeurs/processeur-intel-core-i9-9900k/f-1076443-bx80684i99900k.html";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  //await browser.close();
})();