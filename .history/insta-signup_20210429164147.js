const puppeteer = require("puppeteer");

require("dotenv").config();

const url = "https://www.instagram.com/accounts/emailsignup/";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  // cookie click
  await page.click(".mt3GC > button");

  await page.type("[name=emailOrPhone]", process.env.INSTA_MAIL, {
    delay: 100,
  });
  await page.type("[name=fullName]", process.env.INSTA_NAME, { delay: 100 });
  await page.type("[name=username]", process.env.INSTA_USERNAME, {
    delay: 100,
  });
  await page.type("[name=password]", process.env.INSTA_PWD, { delay: 100 });

  await page.click("button[type=submit]");

  await page.waitForSelector('[title="Mois :"]');
  await page.select('[title="Mois :"]', "2");
  await page.select('[title="Jour :"]', "18");
  await page.select('[title="Année :"]', "1981");

  // await browser.close():
})();
