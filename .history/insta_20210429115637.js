const puppeteer = require("puppeteer");
const nodemailer = require("nodemailer");

require("dotenv").config();

const url =
  "https://www.";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  //await browser.close();
})();