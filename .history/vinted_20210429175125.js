const puppeteer = require("puppeteer");

require("dotenv").config();

const url = "https://www.vinted.fr/";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  await page.setViewport({ width: 1200, height: 1500 });

  // cookie
  await page.click("#onetrust-button-group > button");

  // login
  await page.click('a[href="/member/signup/select_type?ref_url=%2F"]');

  await page.waitForSelector('a[href="/member/general/login?ref_url=%2F"]');
  await page.click('a[href="/member/general/login?ref_url=%2F"]');
  await page.click('div#"checkbox');
})();
