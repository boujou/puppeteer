const puppeteer = require("puppeteer");

const url =
  "https://www.cdiscount.com/informatique/processeurs/processeur-intel-core-i9-9900k/f-1076443-bx80684i99900k.html";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  await page.setViewport({
    width: 1200,
    height: 10000,
  });

  /*     // pdf
  await page.pdf({
    path: "page.pdf",
    format: "A4",
  });
 */

  /*   // image
  await page.screenshot({
      path: "image.png",
  })
 */

  /*   // get body
  let bodyHTML = await page.evaluate(() => document.body.innerHTML);
  console.log(bodyHTML);
 */

  // get data
  let data = await page.evaluate(() => {
    return document.querySelector("span[itemprop=price]").innerText;
  });
  console.log("le prix est de : " + data);

  let newData = data.substring(0, 3);

  if (parseInt(newData) < 380) {
    console.log("Yes !!!le prix est de : " + newData + " euros");
  }

  async function sendNotification(price) {
    let transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "jeanbabtistejosselin@gmail.com",
        pass: process.env.MAIL_PASS,
      },
    });

    let info = await transporter
    .sendMail({
        from: '"PROC CDISCOUNT" <jeanbaptistejosselin@gmail.com>',
        to: "playgtamod@gmail.com",
        subject: "Prix sous les " + price + " Euros",
        html: "Le prix du processeur est en dessous de " + price + " Euros",
    })
    .then(() => )
  }

  await browser.close();
})();
