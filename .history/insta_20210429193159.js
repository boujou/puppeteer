const puppeteer = require("puppeteer");

require("dotenv").config();

const url = "https://instagram.com";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  // cookie click
  await page.click('._1XyCr > button ');

  // login
  await page.type("[name=username]", process.env.MOFO_MAIL, { delay: 200 });
  await page.type("[name=password]", process.env.MOFO_PWD, { delay: 200 });
  await page.click("button[type=submit]");

  await page.click("cmbtv >"")

  //await browser.close();
})();
