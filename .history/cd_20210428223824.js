const puppeteer = require("puppeteer");

const url =
  "https://www.cdiscount.com/informatique/processeurs/processeur-intel-core-i9-9900k/f-1076443-bx80684i99900k.html";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  await page.setViewport({
    width: 1200,
    height: 1000,
  });

  // pdf
  await page.pdf({
      path: "page.pdf",
      format:   
  })
  //await  browser.close();
})();
