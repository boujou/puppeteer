const puppeteer = require("puppeteer");

require("dotenv").config();

const url = "https://www.instagram.com/accounts/emailsignup/";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  // cookie click
  await page.click(".mt3GC > button");

  await page.type("[name=emailOrPhone]", process.env.INSTA_MAIL, { delay: 200 });
  await page.type("name="fullName], process.env.INSTA_MAIL, { delay: 200 });
})();
