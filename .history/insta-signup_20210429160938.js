const puppeteer = require("puppeteer");

require("dotenv").config();

const url = "https://www.instagram.com/accounts/emailsignup/";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  // cookie click
  await page.click(".mt3GC > button");

  await page.type('[name=emailOrPhone]', proc)
})();
