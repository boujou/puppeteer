const puppeteer = require("puppeteer");

const url =
  "https://www.cdiscount.com/informatique/processeurs/processeur-intel-core-i9-9900k/f-1076443-bx80684i99900k.html";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  await page.setViewport({
    width: 1200,
    height: 10000,
  });

  /*     // pdf
  await page.pdf({
    path: "page.pdf",
    format: "A4",
  });
 */

  /*   // image
  await page.screenshot({
      path: "image.png",
  })
 */

  /*   // get body
  let bodyHTML = await page.evaluate(() => document.body.innerHTML);
  console.log(bodyHTML);
 */

  // get data
  let data = await page.evaluate(() => {
    return document.querySelector("span[itemprop=price]").innerText;
  });

  let newData = data.substring

  console.log("le prix est de : " + data);

  await browser.close();
})();
