const puppeteer = require("puppeteer");

require("dotenv").config();

const url = "https://www.vinted.fr/";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  await page.setViewport({ width: 1200, height: 1500 });

  // cookie
  await page.click("#onetrust-button-group > button");

  // login
  await page.click('a[href="/member/signup/select_type?ref_url=%2F"]');

  await page.waitForSelector(
    'a[href="/member/signup/email_start?ref_url=%2F"]'
  );
  await page.click('a[href="/member/signup/email_start?ref_url=%2F"]');

  await page.type('#user_login', process.env.MOFO_USER_NSM)
  await page.type('#user_email', process.env.MOFO_MAIL)
  await page.type('#user_password', process.env.MOFO_MAIL)

  //await browser.close();
})();
