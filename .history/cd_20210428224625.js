const puppeteer = require("puppeteer");

const url =
  "https://www.cdiscount.com/informatique/processeurs/processeur-intel-core-i9-9900k/f-1076443-bx80684i99900k.html";

(async () => {
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page.goto(url, { waitUntil: "networkidle2" });

  await page.setViewport({
    width: 1200,
    height: 10000,
  });

/*   // pdf
  await page.pdf({
    path: "page.pdf",
    format: "A4",
  });

  // image
  await page.screenshot({
      path: "image.png",
  }) */
  
  // get body
  let bodyHTML = await page.evaluate(() => document.body)

  await  browser.close();
})();
